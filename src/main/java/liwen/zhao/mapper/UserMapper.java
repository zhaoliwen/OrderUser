package liwen.zhao.mapper;

import liwen.zhao.domain.Order;
import liwen.zhao.domain.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    User queryUserByUid(String userId);
    
    void addPoint(@Param("orderMoney") Double orderMoney);
}
