package liwen.zhao.mapper;

import liwen.zhao.domain.Order;

public interface OrderMapper {
    Order orderPay(String orderId);
}
