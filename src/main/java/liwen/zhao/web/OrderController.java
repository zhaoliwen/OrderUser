package liwen.zhao.web;

import liwen.zhao.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {
    @Autowired
    UserService userService;
    @RequestMapping("/order/pay")
    public int orderPay(String orderId){
        return userService.orderPay(orderId);
    }
    
}
