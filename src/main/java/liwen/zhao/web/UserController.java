package liwen.zhao.web;

import liwen.zhao.domain.User;
import liwen.zhao.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired(required = false)//找到就注入，找不到就不注入
    UserService userService;
    @RequestMapping("/test.do")
    public void test(){
        System.out.println("test ok:The numbers of folers under antivirus protections");
    }
    
    @RequestMapping("/user/query/point")
    public User queryPoint(String userId){
        User user=userService.queryUserByUid(userId);
        return user;
    }
}
