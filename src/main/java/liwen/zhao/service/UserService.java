package liwen.zhao.service;

import liwen.zhao.domain.User;

public interface UserService {
    User queryUserByUid(String userId);
    
    int orderPay(String orderId);
}
