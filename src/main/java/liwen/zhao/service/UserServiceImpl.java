package liwen.zhao.service;

import liwen.zhao.domain.Order;
import liwen.zhao.domain.User;
import liwen.zhao.mapper.OrderMapper;
import liwen.zhao.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired(required = false)
    UserMapper userMapper;
    @Autowired(required = false)
    OrderMapper orderMapper;
    public User queryUserByUid(String userId) {
        return userMapper.queryUserByUid(userId);
    }
    
    public int orderPay(String orderId) {
        Order order=orderMapper.orderPay(orderId);
        if(order!=null){
            //用户积分+money
            userMapper.addPoint(order.getOrderMoney());
            return 1;
        }else{
            return 0;
        }
    }
}
